package cl.matiml.taote.app.taoteapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;


public class BluetoothDeviceSettings extends AppCompatActivity implements AdapterView.OnItemClickListener {
    public final static String BT_DEVICE_ADDR = "cl.matiml.taote.app.taoteapp.BT_DEVICE_ADDR";

    Context context;
    SharedPreferences sp;

    BluetoothAdapter btAdapter;
    Set<BluetoothDevice> pairedDevices;

    ListView bluetoothDevicesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;
        sp = this.getSharedPreferences("LALALATaote", Context.MODE_PRIVATE);

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothDevicesListView = (ListView) findViewById(R.id.bluetooth_devices_list_view);

        pairedDevices = btAdapter.getBondedDevices();
        ArrayList list = new ArrayList();

        if (pairedDevices.size()>0) {
            for(BluetoothDevice bt : pairedDevices) {
                list.add(bt.getName() + "\n" + bt.getAddress()); //Get the device's name and the address
            }
        } else {
            Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found.", Toast.LENGTH_LONG).show();
        }

        final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, list);
        bluetoothDevicesListView.setAdapter(adapter);
        bluetoothDevicesListView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String info = ((TextView) view).getText().toString();
        String address = info.substring(info.length() - 17);

        SharedPreferences.Editor editor = sp.edit();
        editor.putString(BT_DEVICE_ADDR, address);
        editor.commit();

        Toast.makeText(this, info.substring(0,info.length() - 18) + " configured as device", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
