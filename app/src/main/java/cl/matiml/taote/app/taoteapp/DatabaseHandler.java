package cl.matiml.taote.app.taoteapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    public static final String DATABASE_NAME    = "TaoteDB.db";
    public static final int    DATABASE_VERSION = 1;

    public static final String PACIENTS_TABLE = "tabla_de_destinos";

    // Campos Tabla de Destinos
    public static final String P_NOMBRE    = "nombre";
    public static final String P_DIRECCION = "direccion";
    public static final String P_ID        = "id";
    public static final String P_LATITUDE  = "latitude";
    public static final String P_LONGITUDE = "longitude";

    // Comandos para crear tablas
    private static final String CREATE_DESTINOS_TABLE_COMMAND =
            "CREATE TABLE " +
                    PACIENTS_TABLE + "(" +
                    P_NOMBRE    + " TEXT, " +
                    P_DIRECCION + " TEXT, " +
                    P_ID        + " INTEGER, " +
                    P_LATITUDE  + " DOUBLE, " +
                    P_LONGITUDE + " DOUBLE);";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DESTINOS_TABLE_COMMAND);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PACIENTS_TABLE);
    }

    /*----------------------------------------------------------------------------------------------------*/
    /*------------------------------------------ Escribir Datos ------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------*/












    /*----------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------- Leer Datos --------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------*/







}
