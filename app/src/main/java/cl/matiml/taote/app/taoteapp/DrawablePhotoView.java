package cl.matiml.taote.app.taoteapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.github.chrisbanes.photoview.PhotoView;

public class DrawablePhotoView extends PhotoView {
    Paint linePaint = new Paint();

    public DrawablePhotoView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public DrawablePhotoView(Context context, AttributeSet attributeSet)
    {
        super(context, attributeSet);

        //TODO:
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        //canvas.drawLine(0, 0, 20, 20, p);
        canvas.drawLine(0, getHeight()/2, getWidth(), getHeight()/2, linePaint);
    }

}