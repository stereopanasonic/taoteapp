package cl.matiml.taote.app.taoteapp;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.github.chrisbanes.photoview.PhotoView;
import net.alhazmy13.imagefilter.ImageFilter;

public class MainActivity extends AppCompatActivity {

    protected String TAG = MainActivity.class.getSimpleName();
    protected SharedPreferences sp;
    protected Context context = this;

    protected Button conectar;
    protected ProgressBar spinner;
    protected CheckBox checkBox;
    protected RelativeLayout imageRelativeLayout;
    protected FrameLayout measureFrameLayout;
    protected PhotoView ultrasoundPhotoView;
    protected TextView measureTextView;
    protected ImageView measurePoint1;
    protected ImageView measurePoint2;
    protected ImageView lineImageView;
    protected ListView ecosListView;
    protected Button filtrarButton;
    protected Button medirButton;
    protected ImageButton compartirImageButton;

    protected BluetoothAdapter btAdapter;
    protected BluetoothSocket btSocket;
    protected InputStream inputStream;
    protected OutputStream outputStream;
    protected Runnable btConnectionLifecycle;
    protected Boolean isConnected = false;
    protected Boolean stayConnected = false;

    protected BluetoothThread bluetoothThread;
    protected ProgressDialog receivingImageProgressDialog;

    protected String btDefaultAddress = "00:00:00:00:00:00";
    protected static final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    ArrayAdapter devicesAdapter;

    private int imageWidth;
    private int imageHeight;

    Bitmap lineBitmap;
    Canvas lineCanvas;
    Paint linePaint;
    Paint transparentPaint;
    private int point1Xd;
    private int point1Yd;
    private int point2Xd;
    private int point2Yd;
    private int dTop = 7;
    private int dBottom = 49;
    private int dLeft = 8;
    private int dRight = 39;
    private int centerPadding = 20;
    private int lineStartX;
    private int lineStartY;
    private int lineEndX;
    private int lineEndY;
    private float scale = 1;

    private int up = 0;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            toast(msg.getData().getString("msje"));
        }

        //  To call Handler
        //    Bundle bundle = new Bundle();
        //    bundle.putString("msje", msg);
        //    Message mssg = new Message();
        //    mssg.setData(bundle);
        //    mHandler.sendMessage(mssg);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;
        sp = this.getSharedPreferences("LALALATaote", Context.MODE_PRIVATE);

        conectar = (Button)findViewById(R.id.button_conectar);
        spinner = (ProgressBar)findViewById(R.id.progressBar);
        checkBox = (CheckBox)findViewById(R.id.checkBox);

        imageRelativeLayout = (RelativeLayout) findViewById(R.id.image_container_relative_layout);
        ultrasoundPhotoView = (PhotoView) findViewById(R.id.image_photo_view);

        measureFrameLayout = (FrameLayout) findViewById(R.id.measure_relative_layout);
        measureTextView = (TextView) findViewById(R.id.measure_text_view);
        measurePoint1 = (ImageView) findViewById(R.id.first_x_measure_point);
        measurePoint2 = (ImageView) findViewById(R.id.second_x_measure_point);
        lineImageView = (ImageView) findViewById(R.id.line_image_view);

        ecosListView = (ListView) findViewById(R.id.ecografias_list_view);
        filtrarButton = (Button) findViewById(R.id.filtro_button);
        medirButton = (Button) findViewById(R.id.medir_button);
        compartirImageButton = (ImageButton) findViewById(R.id.compratir_image_button);

        bluetoothThread = new BluetoothThread("myBluetoothThread");
        bluetoothThread.start();
        bluetoothThread.prepareHandler();

        // receivingImageProgressDialog = new ProgressDialog(context);
        // receivingImageProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // receivingImageProgressDialog.setCancelable(false);
        // receivingImageProgressDialog.setProgressNumberFormat(null);

        /*
         * Receive Image Send Intents from other app's
         */
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendImageReceivedIntent(intent); // Handle single image being sent
            } else {
                Log.i(TAG, "Intent received of a non image type");
            }
        } else {
            Log.i(TAG, "Intent received of a non SEND action");
        }

        /*
         * Ask to turn on bluetooth
         */
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter == null) {
            Toast.makeText(getApplicationContext(), R.string.bluetooth_not_supported, Toast.LENGTH_LONG).show();
            finish(); //finish apk
        } else if(!btAdapter.isEnabled()) {
            Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnBTon,1);
        }

        /*
         * Display previous ultrasound images
         */
        File directory = getExternalFilesDir(null);
        File[] files = directory.listFiles();
        if (files.length < 1) {
            /* Save example image */
            Bitmap exampleEco = BitmapFactory.decodeResource(context.getResources(), R.drawable.ecografia_3);
            File exampleFile = new File(getExternalFilesDir(null), "example_eco.png");
            try {
                exampleFile.createNewFile();
                FileOutputStream fos = new FileOutputStream(exampleFile);
                exampleEco.compress(Bitmap.CompressFormat.PNG, 75, fos);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }
            files = new File[]{exampleFile};
        }
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < files.length; i++) {
            String name = files[i].getName();
            list.add(name.substring(0, name.length()-4));
            Log.d("Files", "FileName:" + files[i].getName());
        }
        devicesAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, list);
        ecosListView.setAdapter(devicesAdapter);
        ecosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i(TAG, "item clicked: " + adapterView.getItemAtPosition(i));
                String imageFile = (String) adapterView.getItemAtPosition(i);
                File imgFile = new  File(getExternalFilesDir(null), imageFile + ".png");
                if(imgFile != null && imgFile.exists()) {
                    Bitmap img = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ultrasoundPhotoView.setImageBitmap(img);
                }
            }
        });

        /*
         * Measurement Line setup
         */
        measurePoint1.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int rawX = (int) event.getRawX();
                final int rawY = (int) event.getRawY();

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN: // Touches object
                        FrameLayout.LayoutParams lParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                        point1Xd = rawX - lParams.leftMargin;
                        point1Yd = rawY - lParams.topMargin;

                        measureTextView.setText(R.string.default_measure_distance);
                        break;

                    case MotionEvent.ACTION_UP: // Releives object
                        FrameLayout.LayoutParams layParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                        lineStartX = layParams.leftMargin + centerPadding;
                        lineStartY = layParams.topMargin + centerPadding;

                        double distance = Math.sqrt(Math.pow(lineEndX - lineStartX, 2) + Math.pow(lineStartY - lineEndY, 2)) / 23 / scale;
                        measureTextView.setText(Double.toString(distance).substring(0,5) + " cm");
                        break;

                    case MotionEvent.ACTION_MOVE: // Moves object
                        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();

                        // Erase the previous line
                        lineCanvas.drawLine(layoutParams.leftMargin + centerPadding, layoutParams.topMargin + centerPadding, lineEndX, lineEndY, transparentPaint);

                        layoutParams.leftMargin = rawX - point1Xd;
                        layoutParams.topMargin = rawY - point1Yd;
                        layoutParams.rightMargin = 0;
                        layoutParams.bottomMargin = 0;
                        if (layoutParams.leftMargin > imageWidth - dRight) {
                            layoutParams.leftMargin = imageWidth - dRight;
                        } else if(layoutParams.leftMargin < 1 - dLeft) {
                            layoutParams.leftMargin = 1 - dLeft - centerPadding;
                        }
                        if (layoutParams.topMargin > imageHeight - dBottom) {
                            layoutParams.topMargin = imageHeight - dBottom;
                        } else if (layoutParams.topMargin < 1 - dTop) {
                            layoutParams.topMargin = 1 - dTop - centerPadding;
                        }
                        view.setLayoutParams(layoutParams);

                        lineCanvas.drawLine(layoutParams.leftMargin + centerPadding, layoutParams.topMargin + centerPadding, lineEndX, lineEndY, linePaint);

                        break;
                }
                measureFrameLayout.invalidate();
                return true;
            }
        });

        measurePoint2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int x = (int) event.getRawX();
                final int y = (int) event.getRawY();

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        FrameLayout.LayoutParams lParams = (FrameLayout.LayoutParams) v.getLayoutParams();
                        point2Xd = x - lParams.leftMargin;
                        point2Yd = y - lParams.topMargin;

                        measureTextView.setText(R.string.default_measure_distance);
                        break;

                    case MotionEvent.ACTION_UP:
                        FrameLayout.LayoutParams layParams = (FrameLayout.LayoutParams) v.getLayoutParams();
                        lineEndX = layParams.leftMargin + centerPadding;
                        lineEndY = layParams.topMargin + centerPadding;

                        double distance = Math.sqrt(Math.pow(lineEndX - lineStartX, 2) + Math.pow(lineEndY - lineStartY, 2)) / 23 / scale;
                        measureTextView.setText(Double.toString(distance).substring(0,5) + " cm");
                        break;

                    case MotionEvent.ACTION_MOVE:
                        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) v.getLayoutParams();

                        // Erase the previous line
                        lineCanvas.drawLine(lineStartX, lineStartY, layoutParams.leftMargin + centerPadding, layoutParams.topMargin + centerPadding, transparentPaint);

                        layoutParams.leftMargin = x - point2Xd;
                        layoutParams.topMargin  = y - point2Yd;
                        layoutParams.rightMargin = 0;
                        layoutParams.bottomMargin = 0;
                        if (layoutParams.leftMargin > imageWidth - dRight) {
                            layoutParams.leftMargin = imageWidth - dRight;
                        } else if(layoutParams.leftMargin < 1 - dLeft) {
                            layoutParams.leftMargin = 1 - dLeft;
                        }
                        if (layoutParams.topMargin > imageHeight - dBottom) {
                            layoutParams.topMargin = imageHeight - dBottom;
                        } else if (layoutParams.topMargin < 1 - dTop) {
                            layoutParams.topMargin = 1 - dTop;
                        }
                        v.setLayoutParams(layoutParams);

                        lineCanvas.drawLine(lineStartX, lineStartY, layoutParams.leftMargin + centerPadding, layoutParams.topMargin + centerPadding, linePaint);

                        break;
                }
                measureFrameLayout.invalidate();
                return true;
            }
        });
    }

    void handleSendImageReceivedIntent(Intent intent) {
        Log.i(TAG, "Image received frome SEND intent");
        try {
            Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (imageUri != null) {
                Bitmap im = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                // canvas1.setImageBitmap(im);
                ultrasoundPhotoView.setImageBitmap(im);
            }
        } catch (IOException e) {
            Log.e(TAG, "handling image uri error: " + e.toString());
        }
    }

    /*
     *  onStart
     */
    @Override
    protected void onStart() {
        super.onStart();

        btConnectionLifecycle = new Runnable() {
            @Override
            public void run() {

                while (stayConnected) {
                    int p = 0;

                    /* 0. Pre BT connection */
                    Log.i(TAG, "UI preparation to initiate BT connection");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            conectar.setEnabled(false);
                            checkBox.setVisibility(View.INVISIBLE);
                            spinner.setVisibility(View.VISIBLE);
                        }
                    });

                    /* 1. Bluetooth Connection */
                    Log.i(TAG, "Connecting to BT device (Taote) in background");
                    while (p < 5) {
                        try {
                            String bt_addr = sp.getString(BluetoothDeviceSettings.BT_DEVICE_ADDR, btDefaultAddress);

                            Log.i(TAG, "trying to connect to " + bt_addr);
                            BluetoothDevice hc05 = btAdapter.getRemoteDevice(bt_addr);
                            btSocket = hc05.createInsecureRfcommSocketToServiceRecord(uuid);
                            btAdapter.cancelDiscovery();

                            btSocket.connect();    //Timeout de conexion ~7s
                            inputStream = btSocket.getInputStream();
                            outputStream = btSocket.getOutputStream();
                            isConnected = true;
                            stayConnected = true;
                            Log.i(TAG, "BT Socket connected");
                            p = 10;
                        } catch (IOException e) {
                            Log.e(TAG, e.toString());
                            isConnected = false;
                            stayConnected = false; // this way, after 5 try's it wont keep trying
                            p++;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    toast(R.string.bluetooth_trying_reconection_toast_message);
                                }
                            });
                        }
                    }

                    /* 2. Connection Result */
                    Log.i(TAG, "Post connection attempt UI management");
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (isConnected) {
                                spinner.setVisibility(View.INVISIBLE);
                                checkBox.setVisibility(View.VISIBLE);
                                checkBox.setChecked(true);
                                conectar.setText(R.string.conectar_button_desconectar);
                                conectar.setEnabled(true);
                            } else {
                                spinner.setVisibility(View.INVISIBLE);
                                checkBox.setVisibility(View.VISIBLE);
                                checkBox.setChecked(false);
                                conectar.setText(R.string.conectar_button_desconectar);
                                conectar.setEnabled(true);

                                toast(R.string.bluetooth_trying_reconection_toast_message);
                            }
                        }
                    });


                    /* 3. BT Connection Lifecycle */
                    if (isConnected) {
                        int data;
                        ArrayList<Integer> msg = new ArrayList<>();

                        int msgLength = 160000; /* 80 * 400 */;
                        int msgCounter = 0;

                        int timeoutTimer;       // for transmission timeout
                        int disconnectionTimer; // for disconnection detection
                        up = 0;             // increase timeoutTimer when transmitting

                        boolean transmissionTimeout = false;
                        boolean msgReceived = false;

                        while (true) {
                            try {
                                timeoutTimer = 0;
                                disconnectionTimer = 0;
                                transmissionTimeout = false;

                                /* 1. waiting for data */
                                while (inputStream.available() <= 0 && timeoutTimer <= 50000) {
                                    timeoutTimer = timeoutTimer + up;
                                    disconnectionTimer++;
                                    // Every ~670ms sends a '1', after ~4 atempts this throws error if no connection.
                                    if (disconnectionTimer > 100000) {
                                        outputStream.write(1);
                                        disconnectionTimer = 0;
                                    }
                                }

                                if (timeoutTimer > 50000)
                                    transmissionTimeout = true;

                                /* 2. char received */
                                if (!transmissionTimeout) {
                                    /* 1. Saving data */
                                    data = inputStream.read();
                                    msg.add(data);

                                    msgCounter++;
                                    up = 1;
                                    //Log.i(TAG, msg_counter + " . " + Character.toString((char) data));

                                    /* 2. Displaying info */
                                    if (msgCounter == 1) {
                                        Log.i(TAG, "Receiving message ...");
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                //receivingImageProgressDialog.setMessage(getResources().getString(R.string.progress_message_receiving_image));
                                                //receivingImageProgressDialog.setMax(100);
                                                //receivingImageProgressDialog.show();
                                            }
                                        });
                                        toast("receiving eco image");
                                    }

                                    Float progress = (float) msgCounter * 100 / msgLength;
                                    if ((progress%1) == 0 ) {
                                        //receivingImageProgressDialog.setProgress((int) msgCounter*100/msgLength);
                                        Log.i(TAG, "progress: " + Float.toString(progress));
                                    }

                                    /* 3. Receiving complete message */
                                    if (msgCounter > msgLength - 1) {
                                        msgReceived = true;

                                    }
                                } else if (transmissionTimeout){
                                    Log.d(TAG, "data timeout");
                                    //receivingImageProgressDialog.dismiss();

                                    /* 1. Evaluate Timeout */
                                    int bytesLeft = msgLength - msg.size();
                                    if (bytesLeft > msgLength/2) {
                                        Log.i(TAG, "more than half message is missing");
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                toast(R.string.timeout_lose_data_received);
                                            }
                                        });

                                        Log.i(TAG, "Reinitializing variables");
                                        msg = new ArrayList<>(); msgCounter = 0; up = 0; msgReceived = false;
                                    } else if (bytesLeft > 1000) {
                                        Log.i(TAG, "less than half the message but more than 1000 bytes missing");
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                toast(R.string.timeout_incomplete_transmission);
                                            }
                                        });

                                        Log.i(TAG, "Reinitializing variables");
                                        msg = new ArrayList<>(); msgCounter = 0; up = 0; msgReceived = false;
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                //receivingImageProgressDialog.setProgress(0);
                                                //receivingImageProgressDialog.setIndeterminate(false); //
                                            }
                                        });
                                    } else {
                                        for (int l = 0; l < bytesLeft; l++) {
                                            msg.add(0);
                                        }

                                        msgReceived = true;
                                    }
                                }

                                if (msgReceived) {
                                    Log.i(TAG, "... message received.");

                                    /* 1. Notify message received */
                                    RingtoneManager.getRingtone(
                                            getApplicationContext(),
                                            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                            .play();

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //receivingImageProgressDialog.setMessage(getResources().getString(R.string.progress_message_building_image));
                                            //receivingImageProgressDialog.setProgressPercentFormat(null);
                                            //receivingImageProgressDialog.setIndeterminate(true);
                                        }
                                    });

                                    /* 2. Building ultrasound image */
                                    final String timeStamp = getDateString();
                                    final String msgB = taoteRawDataStreamToString(msg);
                                    final Bitmap testB = taoteRawDataStreamToEcoImage(msg);

                                    //writeMessageToFile(arr_byte, timeStamp);
                                    storeImage(testB, timeStamp);

                                    toast("Eco image ready!");

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ultrasoundPhotoView.setImageBitmap(testB);

                                            devicesAdapter.add("ECO-" + timeStamp);
                                            devicesAdapter.notifyDataSetChanged();
                                            ecosListView.deferNotifyDataSetChanged();

                                        }
                                    });

                                    /* 3. Reinitialize to receive new image */
                                    Log.i(TAG, "Reinitializing variables");
                                    msg = new ArrayList<>(); msgCounter = 0; up = 0; msgReceived = false;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //receivingImageProgressDialog.setProgress(0);
                                            //receivingImageProgressDialog.setIndeterminate(false);
                                            //receivingImageProgressDialog.setProgressNumberFormat("%1d/%2d");
                                            NumberFormat percentInstance = NumberFormat.getPercentInstance();
                                            percentInstance.setMaximumFractionDigits(0);
                                            //receivingImageProgressDialog.setProgressPercentFormat(percentInstance);
                                            //receivingImageProgressDialog.dismiss();
                                        }
                                    });
                                }

                            } catch (IOException e) {
                                Log.e(TAG, e.toString());

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        desconectarUIDispositivoBT();
                                    }
                                });

                                break;
                            }
                        }
                    }

                    if (stayConnected) {
                        try {
                            Log.i(TAG, "Bluetooth Device disconnected");
                            Log.i(TAG, "Reconnecting in 2 seconds");
                            TimeUnit.SECONDS.sleep(2);
                            Log.i(TAG, "Reconnecting bluetooth connection");
                        } catch (InterruptedException e) {
                            Log.e(TAG, e.toString());
                            e.printStackTrace();
                        }
                    }
                }
                Log.i(TAG, "reached end of runnable");
            }
        };
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (up == 0) {
            // Image original size 748x561
            imageWidth = ultrasoundPhotoView.getWidth();
            imageHeight = (int) imageWidth * 561 / 748;
            ultrasoundPhotoView.getLayoutParams().height = imageHeight;
            Log.i(TAG, "Image size: " + imageWidth + ", " + imageHeight);

            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) measureFrameLayout.getLayoutParams();
            layoutParams.height = imageHeight;
            layoutParams.width = imageWidth;
            measureFrameLayout.setLayoutParams(layoutParams);

            lineBitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888);
            lineCanvas = new Canvas(lineBitmap);
            lineImageView.setImageBitmap(lineBitmap);

            linePaint = new Paint();
            linePaint.setColor(getResources().getColor(R.color.spotifyGreen));
            linePaint.setStrokeWidth(3);

            transparentPaint = new Paint();
            transparentPaint.setStrokeWidth(5);
            transparentPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

            measureTextView.setVisibility(View.INVISIBLE);
            lineImageView.setVisibility(View.INVISIBLE);
            measurePoint1.setVisibility(View.INVISIBLE);
            measurePoint2.setVisibility(View.INVISIBLE);
        }
    }

    protected void onDestroy() {
        super.onDestroy();

        stayConnected = false;
        bluetoothThread.quit();
    }

    /*
    *   TAOTE FUNCTIONS
    */
    protected String taoteRawDataStreamToString(ArrayList<Integer> arr_byte) throws UnsupportedEncodingException {
        final String msg;
        try {
            Log.i(TAG, "- parsing message");
            byte[] buffer = new byte[arr_byte.size()];
            for (int i = 0; i < arr_byte.size(); i++) {
                buffer[i] = arr_byte.get(i).byteValue();
            }
            msg = new String(buffer, "US-ASCII");
            Log.i(TAG, "bt msg: " + msg);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "obtainMessage - exception: " + e.toString());
            throw e;
        }

        return msg;
    }

    protected Bitmap taoteRawDataStreamToEcoImage(ArrayList<Integer> arr_byte) {
        // TODO: se puede implementar funcion de im. polar a ecografía en tiempo real.

        /* Polar Image */
        Log.i(TAG, "obtaining polar image");

        int rDim = 2000;
        int thetaDim = 80;
        int color;

        // Se arma imagen de 80x2000
        Bitmap polar = Bitmap.createBitmap(rDim, thetaDim, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < thetaDim; i++) {
            for (int j = 0; j < rDim; j++) {
                color = arr_byte.get(j + rDim * i);
                polar.setPixel(j, i, Color.argb(255, color, color, color));
            }
        }

        /* Scan Image
         *
         * Código traducido de implementación en matlab: scanConverter
         * - matlab trabaja por [filas, columnas]
         * - java por [width, height] y [x, y]
         *
         * - matlab indexa en 1
         * - java indexa en 0
         *
         */
        Log.i(TAG, "obtaining scaned image");
        Double rMin     = 60.0770;
        Double rMax     = 210.1500;
        Double thetaMin = -0.5105;
        Double thetaMax = 0.5236;
        Integer wDim = (int) (640 * 1.17) / 1;
        Integer hDim = (int) (480 * 1.17) / 1;
        Double x0 = 0.0;
        Double y0 = 0.0;
        Double w = 0.0;
        Double h = 0.0;

        Double dR     = (rMax - rMin) / rDim;
        Double dTheta = (thetaMax - thetaMin) / thetaDim;

        if(h == 0 || w == 0) {
            if (Math.abs(Math.sin(thetaMin)) * hDim / wDim > 0.5) {
                w = 2 * Math.abs(rMax * Math.sin(thetaMin)) * 1.05;
                h = hDim * w / wDim;
            } else {
                h = rMax * 1.05;
                w = wDim * h / hDim;
            }
            x0 = -w / 2;
            y0 = 0.0;
        }

        Double xScale = w / wDim;
        Double yScale = h / hDim;

        Bitmap M = Bitmap.createBitmap(wDim,hDim, Bitmap.Config.ARGB_8888);

        for (int x = 1; x <= wDim; x++) { //x = 1:h_dim
            for (int y = 1; y <= hDim; y++) { //y = 1:v_dim
                double x_s = x * xScale + x0;
                double y_s = y * yScale + y0;
                double theta = -Math.atan2(y_s, x_s) + Math.PI / 2;
                double r = Math.sqrt(x_s * x_s + y_s * y_s);

                int thetaIndex1, thetaIndex2, rIndex1, rIndex2;
                double theta1, theta2, r1, r2;

                if (r > rMin + dR && r < rMax && theta > thetaMin + dTheta && theta < thetaMax) {
                    thetaIndex1 = (int) ((theta - thetaMin) / dTheta);
                    thetaIndex2 = thetaIndex1 + 1;
                    rIndex1 = (int) ((r - rMin) / dR);
                    rIndex2 = rIndex1 + 1;

                    theta1 = thetaMin + thetaIndex1 * dTheta;
                    theta2 = theta1 + dTheta;
                    r1 = rMin + rIndex1 * dR;
                    r2 = r1 + dR;

                    color = (int) (
                                      (theta - theta1) * (r - r1) / (dR * dTheta) * Color.blue(polar.getPixel(rIndex2-1, thetaIndex2-1))
                                    + (theta - theta1) * (r2 - r) / (dR * dTheta) * Color.blue(polar.getPixel(rIndex1-1, thetaIndex2-1))
                                    + (theta2 - theta) * (r - r1) / (dR * dTheta) * Color.blue(polar.getPixel(rIndex2-1, thetaIndex1-1))
                                    + (theta2 - theta) * (r2 - r) / (dR * dTheta) * Color.blue(polar.getPixel(rIndex1-1, thetaIndex1-1))
                            );
                    // Log.i(TAG, "color: " + Integer.toString(color));

                    M.setPixel(x-1,y-1, Color.argb(255, color, color, color));
                }
            }
        }

        return M;
    }

    private void storeImage(Bitmap image, String time) {
        File pictureFile = new File(getExternalFilesDir(null), "ECO-" + time + ".png");
        try {
            if (!pictureFile.exists())
                pictureFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 75, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    /*
     *  ON CLICK'S
     */
    public void conectarPressed(View view) {
        Log.i(TAG, "- conectarPressed - in");

        if (!isConnected) {
            if(!btAdapter.isEnabled()) {
                toast(R.string.bluetooth_turn_on_toast_message);

            } else {
                String configuredBTAddr = sp.getString(BluetoothDeviceSettings.BT_DEVICE_ADDR, btDefaultAddress);
                if (configuredBTAddr.equals(btDefaultAddress)) {
                    new AlertDialog.Builder(this)
                            .setTitle("No Taote device configured")
                            .setMessage("Please configure a default bluetooth taote device for connection.")
                            .setPositiveButton("Configure", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    initiateBluetoothSettingsActivity();
                                }
                            })
                            .setNegativeButton("Cancel", null)
                            .show();

                } else {
                    Log.i(TAG, "Iniciando thread");
                    stayConnected = true;
                    bluetoothThread.postTask(btConnectionLifecycle);
                }
            }

        } else {
            Log.i(TAG, "cerrando thread");
            if (btSocket != null) {
                try {
                    stayConnected = false;
                    btSocket.close();
                    desconectarUIDispositivoBT();
                } catch (IOException e) {
                    toast(e.toString());
                }
            }
            // Parece ser una buena idea despues bloquear la desconexion manual, se podria dejar en alguna opcion
            // escondida. Pero que la aplicacion tenga siempre un ecografo asociado al que se conecta automaticamente
            // y se desconecta cuando este se apaga. Asi la conexion se vuelve invisible al usuario.
            // Para cambiar de dispositivo se puede agregar una opcion, pero se dejara que la conexion BT sea invisible
            // al usuario.
        }
        Log.i(TAG, "- conectarPressed - out");
    }

    public void aplicarFiltro(View view) {
        Log.i(TAG, "aplicarFiltro - in");
        Bitmap img = ((BitmapDrawable) ultrasoundPhotoView.getDrawable()).getBitmap();

        //Bitmap filtrada = ImageFilter.applyFilter(img, ImageFilter.Filter.NEON,Color.RED,Color.GREEN,Color.BLUE);
        Bitmap filtrada = ImageFilter.applyFilter(img, ImageFilter.Filter.HDR);

        ultrasoundPhotoView.setImageBitmap(filtrada);
    }

    public void dibujarLinea(View view) {
        if (measureTextView.getVisibility() == View.INVISIBLE) {
            scale = ultrasoundPhotoView.getScale();
            Log.i(TAG, "image scale: " + scale);

            medirButton.setText("listo");
            measureTextView.setVisibility(View.VISIBLE);
            lineImageView.setVisibility(View.VISIBLE);
            measurePoint1.setVisibility(View.VISIBLE);
            measurePoint2.setVisibility(View.VISIBLE);
            ultrasoundPhotoView.setEnabled(false);
            conectar.setEnabled(false);
            filtrarButton.setEnabled(false);
            compartirImageButton.setEnabled(false);
            ecosListView.setEnabled(false);

            lineStartX = imageWidth * 2 / 5;
            lineStartY = imageHeight * 2 / 3;
            lineEndX = imageWidth * 3 / 5;
            lineEndY = imageHeight * 2 / 3;

            lineBitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888);
            lineCanvas = new Canvas(lineBitmap);
            lineImageView.setImageBitmap(lineBitmap);

            FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams) measurePoint1.getLayoutParams();
            layoutParams1.leftMargin = lineStartX - centerPadding;
            layoutParams1.topMargin = lineStartY - centerPadding;
            layoutParams1.rightMargin = 0;
            layoutParams1.bottomMargin = 0;
            measurePoint1.setLayoutParams(layoutParams1);

            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) measurePoint2.getLayoutParams();
            layoutParams2.leftMargin = lineEndX - centerPadding;
            layoutParams2.topMargin = lineEndY - centerPadding;
            layoutParams2.rightMargin = 0;
            layoutParams2.bottomMargin = 0;
            measurePoint2.setLayoutParams(layoutParams2);

            lineCanvas.drawLine(lineStartX, lineStartY, lineEndX, lineEndY, linePaint);

            double distance = Math.sqrt(Math.pow(lineEndX - lineStartX, 2) + Math.pow(lineEndY - lineStartY, 2)) / 23 / scale;
            measureTextView.setText(Double.toString(distance).substring(0,5) + " cm");
        } else {
            medirButton.setText("Medir");
            measureTextView.setVisibility(View.INVISIBLE);
            lineImageView.setVisibility(View.INVISIBLE);
            measurePoint1.setVisibility(View.INVISIBLE);
            measurePoint2.setVisibility(View.INVISIBLE);
            ultrasoundPhotoView.setEnabled(true);
            conectar.setEnabled(true);
            filtrarButton.setEnabled(true);
            compartirImageButton.setEnabled(true);
            ecosListView.setEnabled(true);
        }
    }

    public void compartirImagen(View view) {
        /* Bitmap img = ((BitmapDrawable) canvas1.getDrawable()).getBitmap();
        Parcel parcel = Parcel.obtain();
        Bitmap destinationBitmap;
        img.writeToParcel(parcel, 0);
        destinationBitmap = Bitmap.CREATOR.createFromParcel(parcel);*/

        File imgFile = getLatestModifiedFilefromDir(getExternalFilesDir(null).toString());
        if(imgFile == null || !imgFile.exists()) {
            toast("No available image");
            return;
        }
        Uri uri = Uri.parse("file://"+imgFile.getAbsolutePath());
        Intent share = new Intent(Intent.ACTION_SEND);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        share.setType("image/*");
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(share, "Compartir ultima ecografia"));
    }

    /*
     *  UTILS & OTHERS
     */
    protected void desconectarUIDispositivoBT() {
        isConnected = false;
        conectar.setText("Conectar");
        checkBox.setChecked(false);
    }

    private void toast(int msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    private void toast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    protected String getDateString() {
        return new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date());
    }

    protected void initiateBluetoothSettingsActivity() {
        Intent intent = new Intent(MainActivity.this, BluetoothDeviceSettings.class);
        startActivity(intent);
    }

    private File getLatestModifiedFilefromDir(String dirPath){
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }

    /*
     * Menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            initiateBluetoothSettingsActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}







/* Permisos

public static void verifyStoragePermissions(Activity activity) {
        int REQUEST_EXTERNAL_STORAGE = 1;
        String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

*/

/* Files and objects

protected void writingFileTest() {
    try {
        File traceFile = new File(getExternalFilesDir(null), "TraceFile.txt");
        if (!traceFile.exists()) {
            traceFile.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(traceFile, true));
        writer.write("This is a test trace file.");
        writer.close();
    } catch (IOException e) {
        Log.e(TAG, e.toString());
        Log.i(TAG, "Unable to write to the TraceFile.txt file.");
    }
}

protected ArrayList<Object> fileContentToArray(String fileName) {
    ArrayList<Object> returnlist = null;
    String returnlistString = null;
    try {
        File file = new File(getExternalFilesDir(null), fileName);
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);

        returnlist = (ArrayList<Object>) ois.readObject();
        ois.close();

        // Log.i(TAG, " \n" +
        //         "file length = " + returnlist.size() + ",\n" +
        //         "file array -> " + returnlistString +
        //         "\n ");
    } catch (Exception e) {
        Log.e(TAG, e.toString());
    }

    return returnlist;
}



protected void writeDataArrayToFile(ArrayList<Integer> data, String time) {
    ObjectOutput out = null;
    File file = new File(getExternalFilesDir(null), "d_" + time + ".txt");
    try {
        if (!file.exists()) {
            file.createNewFile();
        }
        out = new ObjectOutputStream(new FileOutputStream(file));
        out.writeObject(data);
        out.close();
    } catch (IOException e) {
        Log.e(TAG, e.toString());
    }
}
*/

/* Filtros de imagenes

public void aplicarFiltro2(View view) {
    Log.i(TAG, "aplicarFiltro - in");
    //Bitmap img = ((BitmapDrawable) canvas1.getDrawable()).getBitmap();
    Bitmap img = ((BitmapDrawable) ultrasoundPhotoView.getDrawable()).getBitmap();

    Bitmap operation = Bitmap.createBitmap(img.getWidth(), img.getHeight(),img.getConfig());

    for(int i=0; i<img.getWidth(); i++){
        for(int j=0; j<img.getHeight(); j++){
            int p = img.getPixel(i, j);
            int r = Color.red(p);
            int g = Color.green(p);
            int b = Color.blue(p);
            int alpha = Color.alpha(p);

            r = 100  +  r;
            g = 100  + g;
            b = 100  + b;
            alpha = 100 + alpha;
            operation.setPixel(i, j, Color.argb(alpha, r, g, b));
        }
    }
    //canvas1.setImageBitmap(operation);
    ultrasoundPhotoView.setImageBitmap(operation);
}

 */
