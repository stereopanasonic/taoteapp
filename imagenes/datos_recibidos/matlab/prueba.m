%% 
% prueba1 = importdata('taote_prueba_1.txt');
% prueba2 = importdata('taote_prueba_2.txt');
% prueba3 = importdata('taote_prueba_3.txt');
% 
% save pruebas_taote prueba1 prueba2 prueba3

load pruebas_taote

%% 
im1 = reshape(prueba1, 2000, 80)';
im2 = reshape(prueba2, 2000, 80)';
im3 = reshape(prueba3, 2000, 80)';
figure(1);
subplot(3,1,1); imshow(im1, [0 255])
subplot(3,1,2); imshow(im2, [0 255])
subplot(3,1,3); imshow(im3, [0 255])

%%
M1 = scanConverter(im1);
M2 = scanConverter(im2);
M3 = scanConverter(im3);
figure(2); imshow(M1, [0 255])
figure(3); imshow(M2, [0 255])
figure(4); imshow(M3, [0 255])

%% Probar robustez a falta de datos
load pruebas_taote
prueba3(end-60000:end) = 0;
im3 = reshape(prueba3, 2000, 80)';
I3 = scanConverter(im3);
figure(5);
subplot(1,3,1); imshow(M3, [0 255])
subplot(1,3,2); imshow(I3, [0 255])
subplot(1,3,3); imshow((M3-I3)-255, [0 255])




