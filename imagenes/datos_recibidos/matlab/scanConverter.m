
function M = scanConverter(polar)
    %%   Par?metros
    r_min       =   60.0770;
    r_max       =   210.1500;
    theta_min   =   -0.5105;
    theta_max   =   0.5236;
    h_dim       =   floor(640*1.17);
    v_dim       =   floor(480*1.17);
    x0          =   0;
    y0          =   0;
    w           =   0;
    h           =   0;

    %%  
    [theta_dim, r_dim] = size(polar)

    dr = (r_max-r_min)/r_dim;
    dtheta = (theta_max-theta_min)/theta_dim;

    if(h == 0 || w == 0)
        if(abs(sin(theta_min))*v_dim/h_dim > 0.5)
            w = 2*abs(r_max*sin(theta_min))*1.05;
            h = v_dim*w/h_dim;
        else
            h = r_max*1.05;
            w = h_dim*h/v_dim;
        end
        x0 = -w/2;
        y0 = 0;
    end

    x_scale = w/h_dim;
    y_scale = h/v_dim;

    M = zeros(v_dim,h_dim);
    %M = 0.6*ones(v_dim,h_dim);
    %M = ones(v_dim,h_dim);

    for x = 1:h_dim
        for y = 1:v_dim
            x_s = x*x_scale+x0;
            y_s = y*y_scale+y0;
            theta = -atan2(y_s,x_s)+pi/2;
            r = sqrt(x_s*x_s+y_s*y_s);

            if(r>r_min+dr && r<r_max && theta>theta_min+dtheta && theta<theta_max)

                theta_index1 = floor((theta-theta_min)/dtheta);
                theta_index2 = theta_index1 + 1;
                r_index1 = floor((r-r_min)/dr);
                r_index2 = r_index1 + 1;

                theta1 = theta_min + theta_index1*dtheta;
                theta2 = theta1 + dtheta;
                r1 = r_min + r_index1*dr;
                r2 = r1 + dr;

                M(y,x) = ((theta-theta1)*(r-r1)/(dr*dtheta)*polar(theta_index2,r_index2)+(theta-theta1)*(r2-r)/(dr*dtheta)*polar(theta_index2,r_index1)+(theta2-theta)*(r-r1)/(dr*dtheta)*polar(theta_index1,r_index2)+(theta2-theta)*(r2-r)/(dr*dtheta)*polar(theta_index1,r_index1));
            end
        end
    end

    M=fliplr(M);
